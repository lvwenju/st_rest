# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/2
# @E-mail : mt_lwj@163.com
# -------------------------
"""
学习requests模块
"""

import requests

# 普通的get请求
# res_get = requests.request("GET", "http://122.51.75.82:8888/demo/get")
# print(res_get.status_code)
# print(res_get.content.decode("utf-8"))

# 带参数的get请求
# res_get_with_para = requests.request("GET", "http://122.51.75.82:8888/demo/getWithPara",
#                                      params={"page": "1", "pagesize": "10"})
# res_get_with_para = requests.get("http://122.51.75.82:8888/demo/getWithPara",params={"page": "1", "pagesize": "10"})
# print(res_get_with_para.status_code)
# print(res_get_with_para.content.decode("utf-8"))


# 普通的post请求
# res_post_02 = requests.request("POST", "http://122.51.75.82:8888/demo/post")
# res_post_02 = requests.post("http://122.51.75.82:8888/demo/post")
# print(res_post_02.status_code)
# print(res_post_02.content.decode('utf8'))

# 带参数的post请求
# res_post_with_para = requests.post("http://122.51.75.82:8888/demo/postWithPara", data={"page": "1", "pagesize": "10"})
# res_post_with_para = requests.request("POST", "http://122.51.75.82:8888/demo/postWithPara",
#                                       data={"page": "1", "pagesize": "10"})
# print(res_post_with_para.status_code)
# print(res_post_with_para.content.decode("utf8"))


# 使用httpbin练习requests模块的使用
# delete请求
import json
res_delete_01 = requests.delete("http://122.51.75.82:1080/anything", headers={"accept": "application/json"})
# res_delete_01 = requests.request("DELETE", "http://122.51.75.82:1080/anything", headers={"accept": "application/json"})

print(res_delete_01.status_code)
print(res_delete_01.json()["url"])
print("\n\n\n")
print(res_delete_01.url)
print("\n\n\n")
print(json.loads(res_delete_01.content.decode("utf8"))["url"])
