# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/1/17
# @E-mail : mt_lwj@163.com
# -------------------------

"""
1、类属性怎么定义？ 实例属性怎么定义？
"""


class ClassName():
    # 类属性
    attr1 = 1
    attr2 = 'ab'

    def __init__(self, name, age, desc):
        # 实例属性
        self.attr3 = {"name": name, "age": age}
        self.attr4 = desc

    # 实例方法，可以被实例调用
    def func1(self, para1, para2):
        print('参数1：{}，参数2：{}'.format(para1, para2))

    # 类方法
    @classmethod
    def func2(cls, str1='...'):
        print('this is func2' + str1)

    # 静态方法
    @staticmethod
    def say_sth(n):
        print("I'll print", n)


if __name__ == '__main__':
    c = ClassName('张三', 18, '这是张三')
    print(ClassName.attr1)
    ClassName.func1(ClassName, '12', 'ab')
    c.func1('23', 'bc')
    c.func2()
    ClassName.func2()
    c.say_sth('.......')
    ClassName.say_sth('.......')
