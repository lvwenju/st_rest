# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/1/29
# @E-mail : mt_lwj@163.com
# -------------------------

## 第1题  #####
"""
用户登录程序需求：
a. 输入用户名和密码
b. 判读用户名和密码是否正确（name=”python”,passwd=”lemonban”）
c. 为了防止暴力破解，登陆仅有三次机会。如果超过三次机会，提示错误次数过多，
账号已被冻结。
"""
def login():
    user_info = {"name": "python", "passwd": "lemonban"}
    for i in range(3):
        input_name = input("请输入用户名：")
        input_pwd = input("请输入密码：")
        if user_info["name"] == input_name and user_info["passwd"] == input_pwd:
            print("登录成功！")
            return True
        else:
            print("用户名或密码输入错误！")
    else:
        print("错误次数过多，账号已被冻结！")
        return False
#
#
# if __name__ == '__main__':
#     login()

## 第2题  #####
"""
 给定一个句子（只包含字母和空格），将句子中的单词位置反转。
a. 比如：”hello xiao mi” > “mi xiao hello”
"""
def reserver_sentences(s):
    res = []
    for words in s.split()[::-1]:
        res.append(words)
    return ' '.join(res)
#
#
# if __name__ == '__main__':
#     s = "hello xiao mi"
#     print(reserver_sentences(s))


## 第3题  #####
"""
 给定一个句子（只包含字母和空格），将句子中的单词位置反转。
a. 比如：”hello xiao mi” > “mi xiao hello”
"""
def sort_list(li: list, reverse=False):
    res = []
    for i in range(len(li)):
        res.append(max(li))
        li.remove(max(li))
    if reverse:
        return res
    else:
        return res[::-1]
#
#
# if __name__ == '__main__':
#     try:
#         x = int(input("请输入第1个数："))
#         y = int(input("请输入第2个数："))
#         z = int(input("请输入第3个数："))
#         l = [x, y, z]
#         print(sort_list(l))
#     except Exception:
#         print('oops 出错了哟~')


## 第4题  #####
"""
编写一个程序，使用 for 循环输出 1-100 之间的偶数
"""
def even_num(end_num: int, start_num: int = 0):
    """
    输出起止数之间的偶数
    :param end_num: 终止数（不含）
    :param start_num: 起始数（默认为0）
    :return: 返回偶数组成的列表
    """
    res = []
    for i in range(start_num, end_num):
        if i % 2 == 0:
            res.append(i)
    return res


# if __name__ == '__main__':
#     print(even_num(100))

## 第5题  ##
"""
打开一个文本文件，读取其内容，把其中的大写字母修改为小写字母，在写入文件覆盖
原内容。
"""


def rewrite_file(file_abspath):
    with open(file_abspath, 'r+', encoding='utf8') as f:
        contents = f.readlines()
        # 移动文件读取指针到开始
        f.seek(0, 0)
        print(contents)
        for content in contents:
            print(content)
            f.write(content.lower())
#
#
# if __name__ == '__main__':
#     rewrite_file("test_file.txt")

## 第6题  #####
"""
现在有一个字符串 s = “asdf2273788hh90999”,请写一段代码来删除字符串中的重复元素。
最后转换为列表保存。
"""
def del_repeat_char(s: str):
    return list(set(s))
#
#
# if __name__ == '__main__':
#     s = "asdf2273788hh90999"
#     print(del_repeat_char(s))

## 第7题  ##
"""
小明有 100 块钱，需要买 100 本书（钱刚好花完），a 类书 5 元一本，b 类书 3 元一本，
c 类书 1 元 2 本。请计算一共有多少种购买的方式。
"""
def buy_book(a, b, c):
    """
    计算购买的组合可能数，以及每种组合汇总的列表
    :param a: a类书籍单价
    :param b: b类书籍单价
    :param c: c类书籍单价
    :return: 购买abc三种书的可能组成的列表
    """
    res = []
    for x in range(1, 100 // a + 1):
        for y in range(1, 100 // b + 1):
            for z in range(1, 200 // int(c*2) + 1):
                if 5 * x + 3 * y + 0.5 * z == 100:
                    res.append([x, y, z])

    return len(res), res
#
#
# if __name__ == '__main__':
#     print(buy_book(5, 3, 0.5))


## 第8题  ##
"""
小明买了一对刚出生的兔子，兔子从出生后第 3 个月起每个月都生一对兔子，生的这对
小兔子涨到第三个月也开始生兔子（每个月生一对兔子），假如兔子都不死，问十个月
后小明的兔子为多少对（思路提示，重点在分析出兔子增长的规律，分析出规则之后通
过 for 循环即可实现）
"""

# # 分析规律得出兔子的个数符合斐波纳挈数列
def calc(n: int = 10):
    """
    计算斐波纳挈数列的第n项的值
    :param n:第n位
    :return: 第n位的值
    """
    base_list = [1, 1]
    if n < 3:
        return base_list[n - 1]
    else:
        return calc(n - 2) + calc(n - 1)
#
#
# if __name__ == '__main__':
#     print(calc())


## 第9题  ##
"""
封装一个函数，实现数据格式转换功能（思路提示：使用for和zip）
将源数据：
cases = [
    ["case_id", "case_title", "url", "data","expected"],
    [1, "用例1", "www.baidu.com", "001", True],
    [2, "用例2", "www.360.cn", "002", False],
    [3, "用例3", "www.163.com", "003", True],
    [4, "用例4", "www.qq.com", "004", False]
]
转换为:
cases02 = [
    {"case_id":1, "case_title":"用例1", "url":"www.baidu.com", "data":"address1", "expected":True},
    {"case_id":2, "case_title":"用例2", "url":"www.360.cn", "data":"address2", "expected":False},
    {"case_id":3, "case_title":"用例3", "url":"www.163.com", "data":"address3", "expected":True},
    {"case_id":4, "case_title":"用例4", "url":"www.qq.com", "data":"address4", "expected":False}
    ]
"""
# cases = [
#     ["case_id", "case_title", "url", "data", "expected"],
#     [1, "用例1", "www.baidu.com", "001", True],
#     [2, "用例2", "www.360.cn", "002", False],
#     [3, "用例3", "www.163.com", "003", True],
#     [4, "用例4", "www.qq.com", "004", False]
# ]
#
#
def format_cases(cases_list: list):
    res = []
    for i in cases_list[1:]:
        res.append(dict(zip(cases_list[0], i)))
    return res
#
#
# if __name__ == '__main__':
#     print(format_cases(cases))

## 第10题  ##
"""
企业发放的奖金根据利润提成：
a. 利润(l)低于或等于 10 万元时，奖金可提 10%
b. 利润高于 10 万元，低于 20 万元时，低于 10 万元的部分按 10%提成，高于 10 万元
的部分可提成 7.5%
c. 20 万到 40 万之间时，高于 20 万元的部分，可提成 5%
d. 40 万到 60 万之间时，高于 40 万元的部分，可提成 3%
e. 60 万到 100 万之间时，高于 60 万元的部分，可提成 1.5%
f. 高于 100 万时，高于 100 万元的部分，可提成 1%
从键盘输入当月利润 l，求应发放的奖金总数
"""

def calc_bonus(l):
    if l <= 10:
        return l * 0.1
    elif 10 < l <= 20:
        return calc_bonus(10) + (l - 10) * 0.075
    elif 20 < l <= 40:
        return calc_bonus(20) + (l - 20) * 0.05
    elif 40 < l <= 60:
        return calc_bonus(40) + (l - 40) * 0.03
    elif 60 < l <= 100:
        return calc_bonus(60) + (l - 60) * 0.015
    else:
        return calc_bonus(100) + (l - 100) * 0.01
#
#
# if __name__ == '__main__':
#     print(calc_bonus(35))
#     print(calc_bonus(110))

## 第11题  ##
"""
编写一个自动售货机，功能如下：
A. 请按照下面的提示选择购买的商品
1).可乐 2.5 元 2).雪碧 2.5 元 3).哇哈哈 3 元 4).红牛 6 元 5).脉动 4 元 6). 果 粒
橙 3.5 元
B. 提示用户投币（支持 1 元、5 元、10 元）
用户输入投币金额；不够商品价格时，继续提示投币；投币超过商品价格则返回商品和
找零，然后结束程序
"""

def vending_machine():
    price_info = {1: 2.5, 2: 2.5, 3: 3, 4: 6, 5: 4, 6: 3.5}
    for_sale = {1: "可乐", 2: "雪碧", 3: "哇哈哈", 4: "红牛", 5: "脉动", 6: "果粒橙"}
    try:
        while True:
            print(
                "------  本店商品种类及价目表  ------\n1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)")
            num = int(input("请输入数字选购商品(输入0退出本程序)："))
            if 0 == num:
                print("欢迎下次光临 ~")
                break
            money_ok_flag = 1
            while True:
                input_money = int(input("请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）："))
                if 0 == input_money:
                    print("欢迎下次光临 ~")
                    money_ok_flag = 0
                    break
                elif input_money not in [1, 5, 10]:
                    print("输入面额不支持，请重新投币 ~")
                    continue
                while input_money < price_info[num]:
                    put_again = int(input("金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）："))
                    if 0 == put_again:
                        print("欢迎下次光临 ~")
                        money_ok_flag = 0
                        break
                    elif put_again not in [1, 5, 10]:
                        print("输入面额不支持，请重新投币 ~")
                        continue
                    input_money += put_again
                if not money_ok_flag: break
                print("您选择的商品是：%s，价格为：%.2f，投币总金额为：%.2f，找零：%.2f" % (
                    for_sale[num], price_info[num], input_money, input_money - price_info[num]))
                break
            if not money_ok_flag: break
    except TypeError:
        print("oops,出错了~")
#
#
# if __name__ == '__main__':
#     vending_machine()
"""
上述代码演示：
------  本店商品种类及价目表  ------
1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)
请输入数字选购商品(输入0退出本程序)：4
请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：10
您选择的商品是：红牛，价格为：6.00，投币总金额为：10.00，找零：4.00
------  本店商品种类及价目表  ------
1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)
请输入数字选购商品(输入0退出本程序)：6
请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：2
输入面额不支持，请重新投币 ~
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：5
您选择的商品是：果粒橙，价格为：3.50，投币总金额为：7.00，找零：3.50
------  本店商品种类及价目表  ------
1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)
请输入数字选购商品(输入0退出本程序)：4
请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：5
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
您选择的商品是：红牛，价格为：6.00，投币总金额为：6.00，找零：0.00
------  本店商品种类及价目表  ------
1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)
请输入数字选购商品(输入0退出本程序)：2
请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：3
输入面额不支持，请重新投币 ~
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
金额不足以支付，请输入投币金额（支持 1 元、5 元、10 元，输入0退出本程序）：1
您选择的商品是：雪碧，价格为：2.50，投币总金额为：3.00，找零：0.50
------  本店商品种类及价目表  ------
1.可乐(2.5元)   2.雪碧(2.5元)  3.哇哈哈(3元)   4.红牛(6元)    5.脉动(4元)    6.果粒橙 (3.5元)
请输入数字选购商品(输入0退出本程序)：0
欢迎下次光临 ~
"""


## 第12题  ##
"""
封装一个老师类
属性：姓名、年龄、性别、授课科目、授课班级（list 类型，可保存多个班级）
方法：添加授课班级、打印老师的信息
"""

class Teacher():
    def __init__(self, name: str, age: int, gender: str, subject: str, classes: list = []):
        self.name = name
        self.age = age
        self.gender = gender
        self.subject = subject
        self.classes = classes

    def add_classes(self, class_name: str):
        self.classes.append(class_name)

    def print_info(self):
        print("姓名：%s，年龄：%d，性别：%s，授课科目：%s，授课班级：%s" % (self.name, self.age, self.gender, self.subject, self.classes))
#
#
# if __name__ == '__main__':
#     t1 = Teacher("张老师", 46, "男", "线性代数", ["19届网络工程1班", "18届化工3班"])
#     t1.print_info()
#     t1.add_classes("20届信息安全2班")
#     t1.print_info()
#
# """
# 上述代码演示：
# 姓名：张老师，年龄：46，性别：男，授课科目：线性代数，授课班级：['19届网络工程1班', '18届化工3班']
# 姓名：张老师，年龄：46，性别：男，授课科目：线性代数，授课班级：['19届网络工程1班', '18届化工3班', '20届信息安全2班']
# """


## 第13题  ##
"""
类和继承
A. 定义一个游戏英雄类（Hero）
属性：名字（name）、血量（HP）
方法：技能 1：移动
B. 定义一个战士类（继承英雄类）
除了上面应用累的属性之外，好多了一个属性：攻击力（attack）和一个方法：技能
2（普通攻击）
C. 定义一个法师类（继承英雄类）
除了上面英雄类的属性之外，还多了一个属性：法力值（MP）和一个方法：技能 2
（法术攻击）
"""

# class Hero(object):
#     def __init__(self, name, hp):
#         self.name = name
#         self.hp = hp
#
#     def move(self):
#         print("I'm %s ,I'm moving ..." % self.name)
#
#
# class Soldier(Hero):
#     def __init__(self, name, hp, attack):
#         self.attack = attack
#         super(Soldier, self).__init__(name, hp)
#         # super().__init__(name,hp)
#
#     def normal_attack(self):
#         print("I'm %s(soldier), I'm attacking(%d) ..." % (self.name, self.attack))
#
#
# class Magus(Hero):
#     def __init__(self, name, hp, mp, attack):
#         self.mp = mp
#         super().__init__(name, hp)
#
#     def spell_attack(self):
#         print("I'm %s(magus), I'm attacking(total mp %d) ..." % (self.name, self.mp))
#
#
# if __name__ == '__main__':
#     s1 = Soldier("soldier1", 300, 120)
#     s2 = Soldier("soldier2", 500, 80)
#     m1 = Magus("magus1", 280, 90, 35)
#     m2 = Magus("magus2", 290, 90, 35)
#     s1.move()
#     s2.normal_attack()
#     m1.move()
#     m2.spell_attack()

"""
# 上述代码演示：
I'm soldier1 ,I'm moving ...
I'm soldier2(soldier), I'm attacking(80) ...
I'm magus1 ,I'm moving ...
I'm magus2(magus), I'm attacking(total mp 90) ...

"""
