# # # !/usr/bin/env  python
# # # -*- coding:utf8 -*-
# # # @Author : panshi
# # # @Time : 2020/1/12
# # # @E-mail : mt_lwj@163.com
# #
# #
# # # 必选参数（位置参数）：必须传入，且位置一一对应
# # def calc(x, y):
# #     print(str(x - y))
# #
# #
# # # 按位置的顺序传入
# # calc(3, 5)  # 结果为：-2
# #
# # # 也可以指定名称传入
# # calc(y=6, x=2)  # 结果为：-4
# #
# #
# # # 必选参数如果不传入会报错TypeError: calc() missing 1 required positional argument: 'y'
# # # calc(3)
# #
# #
# # # 默认参数：指定默认值的参数（可传可不传）
# # def calc(x, y=7):
# #     print(str(x - y))
# #
# #
# # # 默认参数可以传入
# # calc(3, 8)  # 结果为：-5
# #
# # # 默认参数可以不传入
# # calc(1)  # 结果是：-6
# #
# #
# # # 可变长参数：可以传入长度不固定的参数
# # # 可变参数一般用*args表示，传入后会被当做元组来处理
# # def pp(x, y=5, *args):
# #     print(x - y)
# #     print(type(args))
# #     print(args)
# #
# #
# # pp(3, 7, 'a', [1, 2, 3])
# # '''
# # # 执行结果为：
# # -4  # 此处因为默认参数传入了值，所以计算的是3-7=-4
# # <class 'tuple'>  # 可以看到后面的*args被处理成了元组传入
# # ('a', [1, 2, 3])  # 此处为args的值
# # '''
# #
# # pp(3, 9, 'a', [1, 2, 3], 'c')
# # '''
# # # 执行结果为：
# # -6  # 此处因为默认参数传入了值，所以计算的是3-9=-6
# # <class 'tuple'>  # 可以看到后面的*args被处理成了元组传入
# # ('a', [1, 2, 3], 'c')  # 此处为args的值
# # '''
# #
# #
# # # 关键字参数（可变长参数）：以关键字的形式传入的参数
# # # 关键字参数一般用**kwargs来表示，传入后会被当作字典来处理
# # def dd(x, y, *args, **kwargs):
# #     print(str(x - y))
# #     print(args)
# #     print(type(kwargs))
# #     print(kwargs)
# #
# #
# # # 直接传入字典
# # dd(2, 4, 'a', 'c', **{'s': 1, 'w': 5})
# # '''
# # # 执行结果为：
# # -2
# # ('a','c')
# # <class 'dict'>
# # {'s': 1, 'w': 5}  # 关键字参数如果传入字典的话，需要在字典前加**
# # '''
# #
# # # 指定关键字传入
# # dd(2, 8, 'a', n=4, k=['a', 'b'], s=6)
# #
# # '''
# # # 执行结果为：
# # -6
# # ('a',)  # 此处是可变参数（无论传入多少均被当成元组传入）
# # <class 'dict'>  # 关键字参数被当作字典来处理
# # {'n': 4, 'k': ['a', 'b'], 's': 6}  # 此处为关键字参数组成的字典
# # '''
# #
# #
# # # 参数传入顺序：必选参数，默认参数，可变长参数，关键字参数
# # def info(a, b=3, *args, **kwargs):
# #     print(a)
# #     print(b)
# #     print(args)
# #     print(kwargs)
# #
# #
# # info(1, 4, 3, 'a', ['b', 'c'], {'k': 1, 'v': 2}, (5, 's'), g=1, k='aa')
# # '''
# # # 执行结果为：
# # 1
# # 4
# # (3, 'a', ['b', 'c'], {'k': 1, 'v': 2}, (5, 's'))  # 无论传入什么类型，均被存放在元组中
# # {'g': 1, 'k': 'aa'}  # 关键字参数被处理成字典
# # '''
# #
# # info(1, *(4, {'w': 2, 's': 'a'}, 1), **{'k': 1, 'v': 2})
# # '''
# # # 执行结果为：
# # 1
# # 4  # 即使是用*传入，但是也会被拆包，并按位置的顺序赋值给b
# # ({'w': 2, 's': 'a'}, 1)  # 然后把剩下的当作元组传入args
# # {'k': 1, 'v': 2}  # **传入的字典，被当做关键字函数传入
# # '''
#
#
# '''
# # 第一题：现有数据如下
# users_title = ["name", "age", "gender"]
# users_info = [['小明', 18, '男'], ["小李", 19, '男'], ["小美", 17, '女']]
#
# # 要求：将上述数据转换为以下格式
# users = [{'name': '小明', 'age': 18, 'gender': '男'},
#          {'name': '小李', 'age': 19, 'gender': '男'},
#          {'name': '小美', 'age': 17, 'gender': '女'}]
# '''
#
# users_title = ["name", "age", "gender"]
# users_info = [['小明', 18, '男'], ["小李", 19, '男'], ["小美", 17, '女']]
#
#
# # 方法1：使用内置函数zip
# def deal_info_1(user_title, user_info):
#     users = []
#     for info in user_info:
#         users.append(dict(zip(user_title, info)))
#     return users
#
#
# # 方法2：使用dict([元组])的形式生成字典
# def deal_info_2(user_title, user_info):
#     res = []
#     for info in user_info:
#         res.append(dict([(user_title[0], info[0]), (user_title[1], info[1]), (user_title[2], info[2])]))
#     return res
#
#
# if __name__ == '__main__':
#     print(deal_info_1(users_title, users_info))
#     print(deal_info_2(users_title, users_info))

'''
第二题：请封装一个函数，按要求实现数据的格式转换
# 传入参数： data = ["{'a':11,'b':2}", "[11,22,33,44]"]
# 返回结果：res = [{'a': 11, 'b': 2}, [11, 22, 33, 44]]
# 通过代码将传入参数转换为返回结果所需数据，然后返回
'''


# # 使用内置函数eval
# def transfer_1(source):
#     res = []
#     for i in source:
#         res.append(eval(i))
#     return res
#
#
# if __name__ == '__main__':
#     data = ["{'a':11,'b':2}", "[11,22,33,44]"]
#     print(transfer_1(data))

pp = [
    {'name': 'zs', 'age': 200},
    {'name': 'ls', 'age': 300},
    {'name': 'ww', 'age': 180},
    {'name': 'aa', 'age': 20},
    {'name': 'bb', 'age': 80}
]


def less_100(n):
    return n.get('age') < 100


# d = {'name': 'ww', 'age': 180}
# print(less_100(d))
print(list(filter(lambda x: less_100(x), pp)))
print(list(filter(lambda x: x['age'] <= 100, pp)))