# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/4
# @E-mail : mt_lwj@163.com
# -------------------------

import unittest
import HTMLTestRunnerNew

from study.python_base.day_013_unitest import st_02_func_test

# 1.引入测试套
suite = unittest.TestSuite()

# 2.引入加载器
loader = unittest.TestLoader()

# 3.1加载测试模块下的测试方法
suite.addTest(loader.loadTestsFromModule(st_02_func_test))

# 3.2加载测试类下的测试方法
# suite.addTest(loader.loadTestsFromTestCase(st_02_func_test.TestRegister))

# 创建测试运行程序
runner = HTMLTestRunnerNew.HTMLTestRunner(stream=open('report.html', 'wb'), title=u'接口自动化测试报告,测试结果如下：',
                                          description=u'用例执行情况：', tester=None)
runner.run(suite)

'''
测试类方法：在所有的测试方法之前执行一次
Tue Feb  4 21:50:59 2020 - Start Test:test_01_normal_register (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
Tue Feb  4 21:50:59 2020 - Start Test:test_02_repeat_register (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
Tue Feb  4 21:50:59 2020 - Start Test:test_03_register_para_username (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
Tue Feb  4 21:50:59 2020 - Start Test:test_04_register_para_password (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
测试类方法：在所有的测试方法之后执行一次
ok test_01_normal_register (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
ok test_02_repeat_register (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
F  test_03_register_para_username (study.python_base.day_013_unitest.st_02_func_test.TestRegister)
ok test_04_register_para_password (study.python_base.day_013_unitest.st_02_func_test.TestRegister)

Time Elapsed: 0:00:00.002001
'''