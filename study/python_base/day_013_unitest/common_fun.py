# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/4
# @E-mail : mt_lwj@163.com
# -------------------------

import copy


def create_or_modify_data(base_data, update_data, pop_data: list=[]):
    """
    基于原始数据生成新的测试数据
    :param base_data: 基础数据（dict或list）
    :param update_data: 增加或更新的值（dict或list）
    :param pop_data: 需要删除的值（dict的键；list的值）
    :return: 修改后的dict或list
    """
    res = copy.deepcopy(base_data)
    try:
        if not isinstance(base_data, (dict, list)):
            raise TypeError("原始数据格式错误！【%s】" % base_data)
        if isinstance(base_data, dict):
            res.update(update_data)
            for key in pop_data:
                res.pop(key)
        if isinstance(base_data, list):
            res.append(update_data)
            for key in pop_data:
                res.remove(key)
        return res
    except TypeError as error_type:
        raise error_type
    except AttributeError as e:
        raise e
