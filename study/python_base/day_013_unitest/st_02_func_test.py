# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/4
# @E-mail : mt_lwj@163.com
# -------------------------
import unittest
from study.python_base.day_013_unitest.st_01_func import *
from study.python_base.day_013_unitest.common_fun import *


class TestRegister(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base_data = {"username": "张三", "password1": "123456a", "password2": "123456a"}
        print("测试类方法：在所有的测试方法之前执行一次")

    @classmethod
    def tearDownClass(cls):
        print("测试类方法：在所有的测试方法之后执行一次")

    def setUp(self):
        print("在每个测试方法前均会执行一次")

    def tearDown(self):  # 没有清理用户的方法，这里仅作打印演示
        print("在每个测试方法后均会执行一次")

    def test_01_normal_register(self):
        """正常注册"""
        base_data_normal = create_or_modify_data(self.base_data, {"username": "normal_user"})
        res_normal = register(**base_data_normal)
        self.assertEqual(1, res_normal["code"], msg='正常注册成功，错误码正确')
        self.assertIn("成功", res_normal["msg"], msg="正常注册成功，提示信息正确")

    def test_02_repeat_register(self):
        """重复注册"""
        base_data_repeat = create_or_modify_data(self.base_data, {"username": "python26"})
        res_repeat = register(**base_data_repeat)
        self.assertEqual(0, res_repeat["code"])
        # 多用几个断言方法
        self.assertTrue("该账户已存在" == res_repeat["msg"], msg="用户已存在")

    def test_03_register_para_username(self):
        """用户名：可选必选/合法/非法值"""
        # 用户名为空
        base_data_username = create_or_modify_data(self.base_data, {"username": ""})
        res_username = register(**base_data_username)
        expected_res = {"code": 0, "msg": "所有参数不能为空"}
        # 直接对比响应数据
        self.assertEqual(res_username, expected_res, msg="参数不能为空（用户名）")
        # 合法
        for legal_name in ['abcdef', 'abcd0123456789abcd']:  # 此处不考虑字符的合法性
            base_data_name_legal = create_or_modify_data(self.base_data, {"username": legal_name})
            res_legal_name = register(**base_data_name_legal)
            expect_res_name = {"code": 1, "msg": "注册成功"}
            self.assertTrue(expect_res_name == res_legal_name, msg="用户名长度合法【%s】" % legal_name)

        # 非法长度
        for illegal_name in ["abcde", "0123456789abcdefghi"]:
            base_data_name_illegal = create_or_modify_data(self.base_data, {"username": illegal_name})
            res_illegal_name = register(**base_data_name_illegal)
            expect_illegal_name_length = {"code": 0, "msg": "账号和密码必须在6-18位之间"}
            self.assertEqual(expect_illegal_name_length, res_illegal_name, msg="用户名长度不合法【%s】" % illegal_name)

    def test_04_register_para_password(self):
        """密码：可选必选/合法/非法值"""
        # 密码1为空
        base_data_password1 = create_or_modify_data(self.base_data, {"password1": ""})
        res_passowrd1 = register(**base_data_password1)
        expected_res1 = {"code": 0, "msg": "所有参数不能为空"}
        self.assertEqual(expected_res1, res_passowrd1, msg="参数不能为空（密码1）")
        # TODO：这里考虑是否合并到一起，不单独判断两个密码为空
        # 密码2为空
        base_data_password2 = create_or_modify_data(self.base_data, {"password2": ""})
        res_password2 = register(**base_data_password2)
        expected_res2 = {"code": 0, "msg": "所有参数不能为空"}
        self.assertEqual(expected_res2, res_password2, msg="参数不能为空（密码2）")

        # 合法
        for legal_password in ['123456', '0123456789abcdefgh']:  # 此处不考虑字符的合法性
            base_data_password_legal = {"username": "name" + legal_password, "password1": legal_password,
                                        "password2": legal_password}
            res_legal_password = register(**base_data_password_legal)
            expect_res_password = {"code": 1, "msg": "注册成功"}
            self.assertTrue(expect_res_password == res_legal_password, "密码长度合法值【%s】" % legal_password)

        # 非法长度
        for illegal_password in ["12345", "0123456789abcdefghi"]:
            base_data_password_illegal = create_or_modify_data(self.base_data, {"password1": illegal_password,
                                                                                "password2": illegal_password})
            res_illegal_password = register(**base_data_password_illegal)
            expect_illegal_password_length = {"code": 0, "msg": "账号和密码必须在6-18位之间"}
            self.assertEqual(expect_illegal_password_length, res_illegal_password, msg="密码长度不合法【%s】" % illegal_password)

        # 两次密码不一致
        base_data_diff_password = create_or_modify_data(self.base_data, {"password1": "123456+", "password2": "123456"})
        res_diff_password = register(**base_data_diff_password)
        expect_diff_password = {"code": 0, "msg": "两次密码不一致"}
        self.assertEqual(expect_diff_password, res_diff_password, msg="两次密码不一致")


if __name__ == '__main__':
    unittest.main()
