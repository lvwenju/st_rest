# !/usr/bin/env  python
# -*- coding:utf8 -*-
# @Author : panshi
# @Time : 2020/1/1
# @E-mail : mt_lwj@163.com

'''
while/for 循环
while循环，用法：
while 条件：
    代码块

当条件不满足时，循环结束。举例：
i = 0
while i < 3:
    print(i)
    i += 1
以上代码
'''

'''
1、 题目：一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在第10次落地时，共经过多少米？
'''
# s = 0
# h = 100.00
# for i in range(10):
#     if i == 0:
#         s += 100
#         print('第 1 次落地经过的距离为：%.2f' % s)
#     else:
#         s += h
#         h /= 2
#         print('第 %d 次落地经过的距离为：%.2f' % (i + 1, s))

'''
2、猴子第一天摘下若干个桃子，当即吃了一半，还不过瘾，又多吃了一个第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
以后每天早上都吃了前一天剩下的一半  在加一个。到第10天早上想再吃时，见只剩下一个桃子了。
请通过一段通过代码来计算第一天摘了多少个桃子？
'''
# n = 1
# i = 0
# while i < 10:
#     if i == 0:
#         n = 1
#         print(f'第 {10 - i} 天的桃子个数为：%d' % n)
#     else:
#         n = (n + 1) * 2
#         print(f'第 {10 - i} 天的桃子个数为：%d' % n)
#     i += 1


'''
3、使用循环和条件语对剪刀石头布游戏进行升级，提示用户输入要出的拳 ：
石头（1）／剪刀（2）／布（3）/退出（4）
电脑随机出拳比较胜负，显示 用户胜、负还是平局，打印结果，一轮游戏完了之后，重新回到用户输入的步骤
'''
# import random
#
# d = {1: '石头', 2: '剪刀', 3: '布'}
# print('------   石头剪刀布游戏开始   --------\n请按下面提示出拳：')
# while True:
#     print('石头【1】 剪刀【2】 布【3】 退出【4】')
#     num = int(input('请输入你的选项：'))
#     if 4 == num: break
#     random_num = random.randint(1, 3)
#     if num == random_num:
#         res = '平局'
#     elif random_num - 1 == num or random_num + 2 == num:
#         res = '您赢了！'
#     else:
#         res = '您输了！'
#     print('您的出拳为：%s，电脑出拳为：%s,%s' % (d[num], d[random_num], res))

'''
冒泡排序
'''

# l = [3, 2, 7, 4, 8, 3, 11, 25, 2]
# while True:
#     n = 0
#     for i in range(len(l)):
#         if len(l) - 1 == i: break
#         if l[i] > l[i + 1]:
#             l[i], l[i + 1] = l[i + 1], l[i]
#             print(l)
#         else:
#             n += 1
#             continue
#     if len(l) - 1 == n: break

'''
斐波纳挈
'''

n = int((input('请输入一个整数（返回斐波那契数列的前n个数）：')))
l = []
i = 0
while i <= n:
    if i == 0:
        # 为零不输出
        i += 1
        continue
    elif i <= 3:
        # 前三个数确定下来
        l = [1, 1, 2][:i]
    else:
        l.append(l[i - 2] + l[i - 3])
    i += 1
print(l)
