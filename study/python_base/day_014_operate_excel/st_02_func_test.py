# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/4
# @E-mail : mt_lwj@163.com
# -------------------------
import unittest
from study.python_base.day_014_operate_excel.st_01_func import *
from study.python_base.day_014_operate_excel.common_fun import OperateExcel


class TestRegister(unittest.TestCase):
    excel = OperateExcel('aaa.xlsx')

    def __init__(self, method_name, test_data):
        self.test_data = test_data
        super().__init__(methodName=method_name)

    def test_register(self):
        """测试：注册"""
        res = register(**eval(self.test_data["data"]))
        self.assertEqual(eval(self.test_data["expected"]), res, msg='正常注册成功，错误码正确')


if __name__ == '__main__':
    unittest.main()
