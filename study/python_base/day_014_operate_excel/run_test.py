# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/4
# @E-mail : mt_lwj@163.com
# -------------------------

import unittest
import HTMLTestRunnerNew

from study.python_base.day_014_operate_excel.st_02_func_test import TestRegister
from study.python_base.day_014_operate_excel.common_fun import OperateExcel

# 1.引入测试套
suite = unittest.TestSuite()

# 2.逐条添加测试用例
excel = OperateExcel(r'E:\02AutoTest\st_rest\study\python_base\aaa.xlsx')
cases = excel.case_data
for case_data in cases:
    suite.addTest(TestRegister("test_register", case_data))

# 创建测试运行程序
runner = HTMLTestRunnerNew.HTMLTestRunner(stream=open('report.html', 'wb'), title=u'接口自动化测试报告,测试结果如下：',
                                          description=u'用例执行情况：', tester='磐石')
result = runner.run(suite)
print(result.failures)
