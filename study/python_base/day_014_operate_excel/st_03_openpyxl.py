# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/2/5
# @E-mail : mt_lwj@163.com
# -------------------------

import openpyxl

# 打开xlsx
wb = openpyxl.open('cases.xlsx')
ws = wb.active
value_rows = [value for value in ws.values]


def deal_data(row_lists):
    res = []
    for content in row_lists[1:]:
        res.append(dict(zip(row_lists[0], content)))
    for i in res:
        print(i)


deal_data(value_rows)
wb.close()
