# !/usr/bin/env  python
# -*- coding:utf8 -*-
# ========================
# @Author : panshi
# @Time : 2020/1/1
# @E-mail : mt_lwj@163.com
# ========================

'''
一、现在有一个列表 li2=[1，2，3，4，5]，

     第一步：请通过三行代码将上面的列表，改成这个样子 li2 = [0，1，2，3，66，5，11，22，33]，
     第二步：对列表进行升序排序 （从小到大）
     第三步：将列表复制一份进行降序排序（从大到小）

'''
# 第一步：请通过三行代码将上面的列表，改成这个样子 li2 = [0，1，2，3，66，5，11，22，33]，
li2 = [1, 2, 3, 4, 5]
li2.insert(0, 0)
li2[4] = 66
li2.extend([11, 22, 33])
print(li2)  # 结果是：[0, 1, 2, 3, 66, 5, 11, 22, 33]

# 第二步：对列表进行升序排序 （从小到大）
li2.sort()
print(li2)  # 结果是：[0, 1, 2, 3, 5, 11, 22, 33, 66]

# 第三步：将列表复制一份进行降序排序（从大到小）
li3 = li2.copy()
li3.sort(reverse=True)
print(li2)  # 结果是：[0, 1, 2, 3, 5, 11, 22, 33, 66]
print(li3)  # 结果是：[66, 33, 22, 11, 5, 3, 2, 1, 0]


'''
二、定义一个空列表user=[],   分别提示用户输入，姓名，年龄，身高，用户输入完之后，
将输入的信息作为添加的列表中保存，然后按照一下格式输出：
    用户的姓名为：xxx,年龄为：xxx,  身高为：xxx  ,请仔细核对（要求：输出的身高要求保留2位小数）
 '''
user = []
name = input('请输入你的姓名：')
age = input('请输入你的年龄：')
heigh = input('请输入你的身高（只保留两位小数）：')
user.append(name)
user.append(age)
user.append('%.2f' % float(heigh))
print('用户的姓名为：%s,年龄为：%s,  身高为：%s  ,请仔细核对（要求：输出的身高要求保留2位小数）' % (user[0], user[1], user[2]))


'''
三、有下面几个数据 ，
t1 = ("aa",11)      t2= ("bb",22)    li1 = [("cc",11)]
请通过学过的知识点，进行相关操作变为如下字典: {"aa":11,"cc":22,"bb":22}
要注意字典中元素的顺序（使用python3.5以下的同学不用考虑位置）
'''
t1 = ("aa", 11)
t2 = ("bb", 22)
li1 = [("cc", 11)]
d = dict([t1, li1[0], t2])
print(d)

'''
四、有5道题（通过字典来存储数据）： 某比赛需要获取你的个人信息，设计一个程序， 
 1、运行时分别提醒输入 姓名、性别、年龄 ，输入完了，请将数据存储起来，
 2、数据存储完了，然后输出个人介绍，格式如下: 我的名字XXX，今年XXX岁，性别XX，喜欢敲代码
 3、有一个人对你很感兴趣，平台需要您补足您的身高和联系方式； 
 4、平台为了保护你的隐私，需要你删除你的联系方式；
 5、你为了取得更好的成绩， 你添加了一项自己的擅长技能。
'''
info_dict = {}
info_dict['name'] = input('请输入你的姓名：')
info_dict['gender'] = input('请输入你的性别：')
info_dict['age'] = input('请输入你的年龄：')
print('我的名字{0}，今年{2}岁，性别{1}，喜欢敲代码'.format(info_dict['name'], info_dict['gender'], info_dict['age']))
info_dict.update({'phone': '13812341234', 'heigh': 112})
print('增加了身高和联系方式：', info_dict)
info_dict.pop('phone')
print('删除了联系方式：', info_dict)
info_dict['skill'] = ['敲代码', '找bug']
print('增加了个人技能：', info_dict)


'''
五、请指出下面那些为可变类型的数据，那些为不可变类型的数据
1、 (11)    
2、 "111"
3、 ([11,22,33])
4、 {"aa":111}
'''
# 数值为不可变类型
# 字符串为不可变类型
# 列表为可变类型
# 字典为可变类型

# 总结：
# 括号仅作为运算符，如果末尾带英文逗号则为元组
# 不可变类型有：数值、字符串、元组；可变类型有字典、列表


'''
6、请获取下面数据中的token，和reg_name'''
data = {
    "code": 0,
    "msg": "OK",
    "data": {
        "id": 74711,
        "leave_amount": 29600.0,
        "mobile_phone": "13367899876",
        "reg_name": "小柠檬666",
        "reg_time": "2019-12-13 11:12:53.0",
        "type": 0,
        "token_info": {
            "token_type": "Bearer",
            "expires_in": "2019-12-30 22:28:57",
            "token": "eyJhbGciOiJIUzUxMiJ9.eyJtZW1iZXJfaWQiOjc0NzExLCJleHAiOjE1Nzc3MTYxMzd9.eNMtnEWr57iJoZRf2IRsGDWm2GKj9LZc1J2SGRprAwOk7EPoJeXSjJwdh0pcVVJygHmsbh1TashWqFv1bvCVZQ"
        }
    },
    "copyright": "Copyright ll Rights Reserved"
}


print(data['data']['token_info']['token'])
print(data['data']['reg_name'])


'''
7、切片 

        1、li = [1,2,3,4,5,6,7,8,9] 请通过切片得出结果 [3,6,9] 

        2、s = 'python java php',通过切片获取: java

        3 、tu = ('a','b','c','d','e','f','g','h'),通过切片获取 ['g','b'] 
'''
li = [1, 2, 3, 4, 5, 6, 7, 8, 9]
s = 'python java php'
tu = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h')
print('取结果[3,6,9]：', li[2::3])
print('取结果java：', s.split()[1])
print("取结果['g','b'] ：", [tu[-2], tu[1]])
