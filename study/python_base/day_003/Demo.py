# !/usr/bin/env  python
# -*- coding:utf8 -*-
# ========================
# @Author : panshi
# @Time : 2020/1/1
# @E-mail : mt_lwj@163.com
# ========================

"""
总结一下最近学习的几种数据类型及其常用方法：
1、数值：分为整型和浮点数以及复数(暂不考虑)
2、字符串
3、列表
4、元组
5、字典
"""

####    1、数值：分为整型和浮点数以及复数(暂不考虑)；不可变类型    #####
a = 15
b = 5
# bit_length 返回十进制数的二进制长度
print(a.bit_length())  # 15的二进制表示：1111，长度为4
print(b.bit_length())  # 5的二进制表示：101，长度为3

###   数值主要用于算数计算例如：加+，减-，乘*，除/，求幂**，求余%   ###
print(9 + 2)  # 结果：11
print(9 - 2)  # 结果：7
print(9 * 2)  # 结果：18
print(9 / 2)  # 结果：4.5
print(9 ** 2)  # 结果：81
print(9 % 2)  # 结果：1

###   把字符串类型的数值变成数值   ###
s = '123'
print(int(s) + 1)  # 结果是124（通过int把字符串类型的数值转换成数值）
s1 = '12.545'
print(float(s))  # 结果是：123.0（会把数值类型的字符串转换成浮点数，默认整数补充一位小数0）
print(float(s1))  # 结果是：12.545
print(int(s))  # 结果是：123
# print(int(s1))  # 此处会报错ValueError，无法直接把字符串类型的浮点数直接转换成整数
print(int(12.546))  # 结果是：12（会把浮点数通过舍弃小数点后面的数直接转换成整数，不是四舍五入！！！）

####    2、字符串：不可变类型    ####
s1 = 'abef'

###   字符串拼接   ###
print(s1 + 'hallo')  # 结果是：abefhallo
print(s1 * 3)  # 结果是：abefabefabef

## join方法：str.join(iterable),把字符串穿插在后面的可迭代对象中，返回拼接后的str；一般用空格分割可迭代对象，返回拼接的结果  ##
print(s1.join('123'))  # 结果是：1abef2abef3
print('_'.join(s1))  # 结果是：a_b_e_f

## format格式化字符串，把后面的参数按位填充到占位符所在位置  ##
print('这是{1}，今年{0}岁'.format(13, '小明'))  # 结果：这是小明，今年13岁

## encode把字符串按照对应的编码格式转换成bytes字节，供读写操作;decode把bytes字节转成对应的编码格式
print('我是中'.encode('utf8'))  # 结果是：b'\xe6\x88\x91\xe6\x98\xaf\xe4\xb8\xad'
print('我是中'.encode('gbk'))  # 结果是：b'\xce\xd2\xca\xc7\xd6\xd0'
s = b'\xce\xd2\xca\xc7\xd6\xd0'
print(s.decode('gbk'))  # 结果是：我是中

# strip默认移除字符串前后的空格，若传入参数则移除字符串首位的“参数”字符串，返回移除之后的结果
s1 = '  abfgcd efg'
print(s1.strip())  # 结果是：abfgcd efg
print(s1.strip('fg'))  # 结果是：  abfgcd e

# upper把所有字符的小写字母转换成大写
s1 = '我是abcd你A呢'
print(s1.upper())  # 结果是：我是ABCD你A呢

# title把字符串每个单词首字母大写；capitalize把字符串第一个字母大写
s1 = 'this is 一只 dog'
s2 = '这 is dog'
print(s1.title())  # 结果是：This Is 一只 Dog
print(s1.capitalize())  # 结果是：This is 一只 dog
print(s2.title())  # 结果是：这 Is Dog
print(s2.capitalize())  # 结果是：这 is dog

# find 查找子字符串所在的位置，如果找到多个则返回从左侧匹配到的第一字符所在的位置（下标0开始），如果找不到则返回-1
s1 = 'abcd12abc2cde1ad'
print(s1.find('d'))  # 结果是：3
print(s1.find('cd'))  # 结果是：2
print(s1.find('as'))  # 结果是：-1

# replace('原字符串'，'新字符串', 次数) 使用新字符串替换原字符串。次数是从左开始匹配到的次数，不传默认替换所有；未匹配到则不替换；
# 替换完成后返回新的字符串（原始字符串不会更改）
s1 = 'abcd12abc2cde1ad'
print(s1.replace('bc', '123'))  # 结果是：a123d12a1232cde1ad
print(s1.replace('ab', '123', 1))  # 结果是：123cd12abc2cde1ad（指定了替换的次数）
print(s1)  # 结果是：abcd12abc2cde1ad（原始字符串不会更改）
s2 = 'abcd12abc2cde1ad'
print(s2.replace('d1d', '123'))  # 结果是：abcd12abc2cde1ad（未找到则不替换）

# split字符串分割，返回分割后的字符串组成的列表（原始字符串不会更改）；不传参数则默认以空格分割（多个空格算一个）；可以指定分割的次数，默认全部分割
s1 = 'abcd 12abc2   debc1ad '
print(s1.split())  # 结果是：['abcd', '12abc2', 'debc1ad']
print(s1.split('bc'))  # 结果是：['a', 'd 12a', '2   de', '1ad ']
print(s1.split('bc', 2))  # 结果是：['a', 'd 12a', '2   debc1ad ']
print(s1.split('cde'))  # 结果是：['abcd 12abc2   debc1ad ']

# count返回字符串中出现的次数;可以指定匹配的起止位置
s1 = 'abcd12abc2cde1ad'
print(s1.count('c'))  # 结果是：3
print(s1.count('cd'))  # 结果是：2
print(s1.count('cda'))  # 结果是：0
print(s1.count('c', 2, 9))  # 结果是：2

#########   字符串切片   ##################

# [n,m,l] n:起始下标，m:终止下标（元素不含在内），l:步长
s = 'abcdefghijklmn'

print(s[0])  # 结果是：a
print(s[2:6])  # 结果是：cdef
print(s[2:7:2])  # 结果是：ceg
print(s[::-1])  # 结果是：nmlkjihgfedcba，此处可以用来处理反转字符串
print(s[2:7][::-2])  # 结果是：gec

############# 3、列表：可变类型 ###################
l1 = [1, 'a', 1, 'st1', 4]

# count返回对象在列表中出现的次数
l1.count('1')  # 结果是2

# append把对象当作一个值添加到列表中；extend把可迭代对象一个个添加到列表中；原始列表会更新
l1.append('ab')
print(l1)  # 结果是：[1, 'a', 1, 'st1', 4, 'ab']
l1.append(['c', 3])
print(l1)  # 结果是：[1, 'a', 1, 'st1', 4, 'ab', ['c', 3]]
l1.extend('abcd')
print(l1)  # 结果是：[1, 'a', 1, 'st1', 4, 'ab', ['c', 3], 'a', 'b', 'c', 'd']
l1.extend(['c', 3])
print(l1)  # 结果是：[1, 'a', 1, 'st1', 4, 'ab', ['c', 3], 'a', 'b', 'c', 'd', 'c', 3]

# clear清除列表的数据
l2 = [1, 'a', 1, 'st1', 4]
l2.clear()
print(l2)

# copy复制列表(浅拷贝)，返回一个新列表；原始列表更改不会影响复制出来的列表
l2 = [1, 'a', 1, 'st1', 4]
l3 = l2
l4 = l2.copy()
l2.append(5)
print(l3)  # 结果是：[1, 'a', 1, 'st1', 4, 5]
print(l4)  # 结果是：[1, 'a', 1, 'st1', 4]
l2.append('b')
print(l3)  # 结果是：[1, 'a', 1, 'st1', 4, 5, 'b']
print(l4)  # 结果是：[1, 'a', 1, 'st1', 4]

##### 延伸学一下深拷贝deepcopy  #######
l = [1, 2, 3, 'a']
# 浅拷贝，拷贝一份新的列表（id会改变）
l1 = l.copy()
print(id(l))  # 结果是：6656368
print(id(l1))  # 结果是：6656488
# 如果改变原始列表，不会影响到新列表
l[1] = 'b'
print(l)  # 结果是：[1, 'b', 3, 'a']
print(l1)  # 结果是：[1, 2, 3, 'a']，因为列表层级就一层，所以浅拷贝不会受到原始列表的改变影响

# 但是如果原始列表层级比较深
l = [1, 2, ['a', 'b']]
l1 = l.copy()
print(l)  # 结果是：[1, 2, ['a', 'b']]
print(l1)  # 结果是：[1, 2, ['a', 'b']]
l[2][1] = 'c'
print(l)  # 结果是：[1, 2, ['a', 'c']]
print(l1)  # 结果是：[1, 2, ['a', 'c']]，此时浅拷贝的列表深层的数据会随着原始列表的更改而改变

# 但是如果使用深拷贝
import copy

l = [1, ['a', 'b']]
l1 = copy.deepcopy(l)
print(l)  # 结果是：[1, ['a', 'b']]
print(l1)  # 结果是：[1, ['a', 'b']]
l[1][1] = 'c'
print(l)  # 结果是：[1, ['a', 'c']]
print(l1)  # 结果是：[1, ['a', 'b']]，此处的值并不会因为原始列表的改变而改变

# index返回值在列表中的索引；可以匹配指定列表的起止位置
l2 = [1, 'a', 'b', 1, 'st1', 4]
print(l2.index(1))  # 结果是：0
print(l2.index(1, 1))  # 结果是：3

# 指定索引插入值
l2 = [1, 'a', 'b', 1, 'st1', 4]
l2.insert(2, 'e')
print(l2)  # 结果是：[1, 'a', 'e', 'b', 1, 'st1', 4]

# pop默认删除列表的最后一个值；可以指定索引删除值
l2 = [1, 'a', 'b', 1, 'st1', 4]
l2.pop()  # 默认删除列表的最后一个值；如果超出列表的索引，报错IndexError
print(l2)  # 结果是：[1, 'a', 'b', 1, 'st1']
l2.pop(2)  # 删除指定索引的值
print(l2)  # 结果是：[1, 'a', 1, 'st1']

########   删除列表的数据   ##############

# remove从左侧移除列表中的值；如果不存在则报错ValueError；
l2 = [1, 'a', 'b', 1, 'st1', 4]
l2.remove(1)  # 从左侧开始移除列表中的值
print(l2)  # 结果是：['a', 'b', 1, 'st1', 4]
del l2[2]
print(l2)  # 结果是：['a', 'b', 'st1', 4]

# sort列表排序，列表的值需要是全数字或者全字母;可以指定排序方式，升序或者降序
l3 = [1, 12, 234, 21, 123]
l3.sort()
print(l3)  # 结果是：[1, 12, 21, 123, 234]
l3.sort(reverse=True)
print(l3)  # 结果是：[234, 123, 21, 12, 1]
l4 = ['a', 'ab', 'acf', 'bc', 'bca']
l4.sort()
print(l4)  # 结果是：['a', 'ab', 'acf', 'bc', 'bca']
l4.sort(reverse=True)
print(l4)  # 结果是：['bca', 'bc', 'acf', 'ab', 'a']

###########   4、元组（不可变类型）  不支持元素的增删改   ###################
# 当元组内只有一个元素时，需要使用英文逗号","结尾，例如(1,)、('abcde',)
t = (1, 2, 'a')

# index获取元素在元组中的下标，如果找不到，则抛异常ValueError
print(t.index(2))
# print(t.index('bc'))  # 此处抛异常：ValueError: tuple.index(x): x not in tuple

# count获取元素在元组中的个数
t = (1, 2, 'a', 1)
print(t.count(1))

# 切片获取元组的值
t = (1, 2, 'a', 1)
print(t[2:])  # 结果是：('a', 1)

###########   5、字典：以键值对的方式存储的数据（可变类型）    ###########
# 键：只能为不可变类型，一般使用字符串
# 多个键的名称相同，值会获取最后一个


### 定义：
d = {'name': '张三', 'age': 18, 'gender': 'man'}
d1 = dict([('ip', '1.2.3.4'), ('port', 8443), ('desc', 'this is desc')])
print(type(d1))  # 结果是：<class 'dict'>

# 直接增加字典的元素
d = {'name': '张三', 'age': 18, 'gender': 'man'}
d['phone'] = '13812341234'
print(d)  # 结果是：{'name': '张三', 'age': 18, 'gender': 'man', 'phone': '13812341234'}

# 字典增加多个元素
d = {'name': '张三', 'gender': 'man'}
d1 = {'age': 18, 'address': '合肥市'}
d.update(d1)
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}

# 增加的字典与当前字典的key相同，则把新的value重新复赋值
d = {'name': '张三', 'gender': 'man'}
d1 = {'age': 18, 'name': '李四'}
d.update(d1)
print(d)  # 结果是：{'name': '李四', 'gender': 'man', 'age': 18}

# 若字典存在不同value的key，则默认使用最后一个value（定义的时候不会报错，但是尽量避免此种情况发生！key一定不要重复！！！）
d = {'name': '张三', 'age': 18, 'gender': 'man', 'age': 20}
print(d['age'])  # 结果是：20

# 方法二：
d = {'name': '张三', 'gender': 'man'}
d.update([('age', 18), ('address', '合肥市')])
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}

## pop删除字典的值，如果不指定键，则默认删除最后一个值；若传入的键不存在则报错KeyError；另外可以传入默认值作返回值，避免因为key不存在导致异常
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
value = d.pop('address')  # 指定删除某个键值对，传入键名称，返回删除的键对应的值
print(value)  # 结果是：合肥市
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'age': 18}

# key不存在则抛异常
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
# v = d.pop('hahha')  # 此处抛异常：KeyError: 'hahha'
v = d.pop('bbbb', 'ab12')  # 此处指定了默认返回值，若key不存在，则返回默认值
print(v)  # 结果是：ab12

#  方法二
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
del d['address']  # 此处直接删除键值对，无返回值
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'age': 18}

## popitem随机删除key及其对应的value；返回一个元组；若字典为空，则抛异常（没有默认值这一说）；是随机删除！！！
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
k, v = d.popitem()
print(d)
print(k, v)
d = {}
# d.popitem()  # 抛异常：KeyError: 'popitem(): dictionary is empty'

## 修改字典的value  ##
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
d['age'] = 20
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'age': 20, 'address': '合肥市'}

# 也可以使用update方法来更新
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
d.update({'name': '李四', 'age': 25})
print(d)  # 结果是：{'name': '李四', 'gender': 'man', 'age': 25, 'address': '合肥市'}

### 常用方法

# copy浅拷贝，浅层数据修改，新对象不会收原始数据的更改而更改
d = {'name': '张三', 'gender': 'man', 'age': 18, 'address': '合肥市'}
d1 = d.copy()
d['name'] = '李四'
print(d)
print(d1)

# 但是如果是多层，新对象会随着原始数据的更改而改变，如果不想被改变就是用copo.deepcopy（），前面列表小节有介绍，此处不再赘述
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
d1 = d.copy()
d['info']['age'] = 20
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'info': {'age': 20, 'address': '合肥市'}}
print(d1)  # 结果是：{'name': '张三', 'gender': 'man', 'info': {'age': 20, 'address': '合肥市'}}，此处数据改变了

## clear清除字典数据
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
d.clear()
print(d)  # 结果是：{}

## get获取key对应的value值；若key不存在则返回None；可以指定默认值，若key不存在则反会默认值
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
n = d.get('name')
print(n)  # 结果是：张三
a = d.get('addasdas')  # 如果不存在key，则返回None
print(a)  # 结果是：None
b = d.get('abcd', 'hahaha')  # 如果不存在key，可以指定默认值，如果获取不到key则返回默认值
print(b)  # 结果是：hahaha

## setdefault设置key对应的默认值，如果key不存在则给字典添加key对应默认值;如果存在key则更新key对应的value（对比着get方法去记忆，一个改默认值一个不改）
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
d.setdefault('hallo', 'world')
print(d)  # 结果是：{'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}, 'hallo': 'world'}

d.setdefault('name', '备用名')
print(d)  # 结果是：{'name': '备用名', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}, 'hallo': 'world'}

# 举个网上的例子，来看看他的作用：
l = [('k1', 1), ('k1', 2), ('k1', 3), ('k2', 'a'), ('k2', 'b'), ('k3', '+'), ('k3', '-'), ('k3', '*')]
d = {}
for k, v in l:
    d.setdefault(k, []).append(v)
print(d)  # 结果是：{'k1': [1, 2, 3], 'k2': ['a', 'b'], 'k3': ['+', '-', '*']}，用在数据统计中很方便

##  keys返回字典的key列表，类型为：<class 'dict_keys'>，一般用作遍历使用
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
print(type(d.keys()))  # 结果是：<class 'dict_keys'>
print(d.keys())  # 结果是：dict_keys(['name', 'gender', 'info'])
for k in d.keys(): print(d[k])
# 上方代码输出：
'''
张三
man
{'age': 18, 'address': '合肥市'}
'''

##  values返回字典的value列表，类型为：<class 'dict_values'>，一般用作遍历使用
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
print(type(d.values()))  # 结果是：<class 'dict_values'>
print(d.values())  # 结果是：dict_values(['张三', 'man', {'age': 18, 'address': '合肥市'}])
for v in d.values(): print(v)
# 上方代码输出：
'''
张三
man
{'age': 18, 'address': '合肥市'}
'''

## items返回字典的键值对，类型为：<class 'dict_items'>，一般用作遍历使用
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
print(d.items())  # 结果是：dict_items([('name', '张三'), ('gender', 'man'), ('info', {'age': 18, 'address': '合肥市'})])
print(type(d.items()))  # 结果是：<class 'dict_items'>
for k, v in d.items(): print('key is : %s, value is : %s' % (k, v))
# 上方代码输出：
'''
key is : name, value is : 张三
key is : gender, value is : man
key is : info, value is : {'age': 18, 'address': '合肥市'}
'''

## fromkeys快速通过对可迭代对象赋值，并组成键值对，返回一个新字典;此方法为dict类型所有，返回值和dict对应的实例无关（dict及其实例可以使用此方法）
d = {'name': '张三', 'gender': 'man', 'info': {'age': 18, 'address': '合肥市'}}
d1 = d.fromkeys(['a', 'b', 'c'])
print(d1)  # 结果是：{'a': None, 'b': None, 'c': None}
d2 = d.fromkeys(['a', 'b', 'c'], 123)
print(d2)  # 结果是：{'a': 123, 'b': 123, 'c': 123}

