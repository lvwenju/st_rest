# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/1/15
# @E-mail : mt_lwj@163.com
# -------------------------

"""
第一题
1、实现一个文件复制器函数，通过给函数传入一个路径，复制该路径下面所有的文件(目录不用复制)到当前目录
要求：如果传路径不存在，不能报错
提示：os模块结合文件读写操作 、异常捕获 即可实现
"""
# import os
#
#
# def file_copy(file_path):
#     """
#     根据传入的路径把路径下的文件拷贝到当前目录，隐藏文件、文件夹不需要复制；复制完成的文件以cp_文件名命名
#     :param file_path:传入文件路径
#     :return: 无
#     """
#     BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#     try:
#         # 切换工作路径
#         os.chdir(file_path)
#         for item in os.listdir(file_path):
#             # isfile只会判断当前目录下是否存在和待比较字符串相同名称的文件
#             # 如果当前工作路径下没有和待比较字符串相同的文件，即使是在其他的目录下有这个相同名称的文件，也会返回False
#             # 过滤目录和隐藏文件
#             if os.path.isfile(item) and not item.startswith('.'):
#                 # 二进制读取方式打开文件
#                 with open(item, 'rb') as read_file:
#                     # 按行读取，避免文件过大导致内存溢出
#                     for content in read_file.readlines():
#                         # 使用绝对路径新建文件并以追加方式写入文件内容
#                         with open(os.path.join(BASE_PATH, 'cp_' + item), 'ab') as write_file:
#                             write_file.write(content)
#
#     except FileNotFoundError:
#         print('文件目录不存在~')
#
#
# if __name__ == '__main__':
#     file_copy(r'E:\02AutoTest\st_rest\study\python_base\day_009_文件操作')


"""
第二题：
改善上节课扩展作业的注册程序，打开文件的读取数据的时候，如果文件不存在会报错。
请通过try-except来捕获这个错误，进行处理，让注册程序可以继续运行。

下方是上节课注册的题目：
第四题：扩展题（不要求提交，有时间的同学可以去思考一下）：
之前作业写了一个注册的功能，再之前的功能上进行升级，
要求：把所有注册成功的用户数据放到文件中进行保存，确保下一次运行代码的时候，上一次运行注册的账号数据还在
"""


def get_user(user_name) -> bool:
    '''
    :param user_name:用户名
    :return: 存在此用户返回True，否则返回False
    '''
    try:
        name_list = []
        with open('test.txt', 'r', encoding='utf8') as f:
            contents = f.readlines()
            name_list = [eval(i)['name'] for i in contents]
        return True if user_name in name_list else False
    except FileNotFoundError:
        print('存储用户信息的文件不存在~')
        # 这里返回Flase是为了程序继续往下执行，可以继续注册
        return False
    ex


def save_info(reg_name, reg_passwd, reg_address) -> bool:
    '''
    :param name:注册的用户名
    :param passwd: 注册的密码
    :param address: 注册填写的地址
    :return: 成功返回True，否则返回False
    '''
    with open('test.txt', 'a', encoding='utf8') as f:
        f.write('\n' + str({'name': reg_name, 'pwd': reg_passwd, 'address': reg_address}))
    return True if get_user(reg_name) else False


def regist():
    while True:
        reg_name = input('请输入用户名：')
        password = input('请输入密码：')
        confirm_password = input('请重复输入密码：')
        reg_address = input('请输入地址：')
        if get_user(reg_name):
            print('此用户名已存在，请重新输入~')
            continue
        elif password != confirm_password:
            print('两次输入的密码不一致，请重新输入~')
        elif save_info(reg_name, password, reg_address):
            print('注册成功！')
            break


if __name__ == '__main__':
    regist()
