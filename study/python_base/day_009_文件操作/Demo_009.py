# !/usr/bin/env  python
# -*- coding:utf8 -*-
# @Author : panshi
# @Time : 2020/1/13
# @E-mail : mt_lwj@163.com

f = open('../abc.txt', 'r', encoding='utf8')
print(f.read())
f.close()

print('---------------')

f = open('../abc.txt', 'r', encoding='utf8')
print(f.readline())
print(f.readline())
f.close()

print('---------------')

f = open('../abc.txt', 'r', encoding='utf8')
print(f.readlines())
f.close()


f = open('test.txt', 'w+', encoding='utf8')
f.write('这是一\n然后才是二')  # 直接把字符串写入文件（遇到换行符直接换行）
f.close()

f = open('test.txt', 'w+', encoding='utf8')
# 可以一次性写入多行（多行内容可以保存在可迭代对象中），遇到换行符\n则换行输入。可与readlines对比记忆
f.writelines(['hello world\n', "I'm python"])
f.close()


'''
第一题：当前有一个txt文件，内容如下：
数据aaa
数据bbb
数据ccc
数据ddd
# 要求：请将数据读取出来，保存为以下格式
{'data0': '数据aaa', 'data1': '数据bbb', 'data2': '数据ccc', 'data3': '数据ddd'}
# 提示：
# 可能会用到内置函数enumerate
# 注意点：读取出来的数据如果有换行符'\n'，要想办法去掉。
'''


def load_file(file_name):
    res = {}
    with open(file_name, 'r', encoding='utf8') as f:
        contents = f.readlines()
        for i in range(len(contents)):
            res['data' + str(i)] = contents[i].rstrip('\n')
    return res


if __name__ == '__main__':
    print(load_file('test.txt'))

#
'''
第二题：当前有一个case.txt文件，里面中存储了很多用例数据: 如下，每一行数据就是一条用例数据
url:www.baidu.com,mobilephone:13760246701,pwd:123456
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555

# 要求一： 请把这些数据读取出来，到并且存到list中，格式如下
[
{'url': 'www.baidu.com', 'mobilephone': '13760246701', 'pwd': '123456'}, {'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},
{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'}
]
​
# 提示：可以分析读取出来的每一行字符串中的内容，然后使用的字符串分割方法进行分割，想办法组装成字典。
# 注意点：数据中如果有换行符'\n'，要想办法去掉。

'''


def deal_data(file_name):
    '''
    :param file_name:文件名或者文件绝对路径
    :return: 文件每行内容组成的字典构成的列表，例如[{'name':'zhangsan','age':16},{'name':'lisi','age':19}]
    '''
    res = []
    with open(file_name, 'r', encoding='utf8') as f:
        contents = f.readlines()
        for content in contents:
            # 去除换行，并得到每行的结果['url:www.baidu.com', 'mobilephone:13760246701', 'pwd:123456']
            temp_list = content.strip('\n').split(',')
            temp_dict = {}
            for item in temp_list:
                # 把上面每行结果的列表转换成字典{'url': 'www.baidu.com', 'mobilephone': '13760246701', 'pwd': '123456'}
                temp_dict[item.split(':')[0]] = item.split(':')[1]
            res.append(temp_dict)
    return res


if __name__ == '__main__':
    print(deal_data('test.txt'))

'''
第四题：扩展题（不要求提交，有时间的同学可以去思考一下）：
之前作业写了一个注册的功能，再之前的功能上进行升级，
要求：把所有注册成功的用户数据放到文件中进行保存，确保下一次运行代码的时候，上一次运行注册的账号数据还在
'''


def get_user(user_name) -> bool:
    '''
    :param user_name:用户名
    :return: 存在此用户返回True，否则返回False
    '''
    name_list = []
    with open('test.txt', 'r', encoding='utf8') as f:
        contents = f.readlines()
        name_list = [eval(i)['name'] for i in contents]
    return True if user_name in name_list else False


def save_info(reg_name, reg_passwd, reg_address) -> bool:
    '''
    :param name:注册的用户名
    :param passwd: 注册的密码
    :param address: 注册填写的地址
    :return: 成功返回True，否则返回False
    '''
    with open('test.txt', 'a', encoding='utf8') as f:
        f.write('\n' + str({'name': reg_name, 'pwd': reg_passwd, 'address': reg_address}))
    return True if get_user(reg_name) else False


def regist():
    while True:
        reg_name = input('请输入用户名：')
        password = input('请输入密码：')
        confirm_password = input('请重复输入密码：')
        reg_address = input('请输入地址：')
        if get_user(reg_name):
            print('此用户名已存在，请重新输入~')
            continue
        elif password != confirm_password:
            print('两次输入的密码不一致，请重新输入~')
        elif save_info(reg_name, password, reg_address):
            print('注册成功！')
            break


if __name__ == '__main__':
    regist()
