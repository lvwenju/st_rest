# !/usr/bin/env  python
# -*- coding:utf8 -*-
# @Author : panshi
# @Time : 2020/1/9
# @E-mail : mt_lwj@163.com


'''
 9、实现一个注册的流程的函数，调用函数就执行下面要求功能
基本要求：
1、运行程序，提示用户，输入用户名，输入密码，再次确认密码。
2、判读用户名有没有被注册过，如果用户名被注册过了，那么打印结果该用户名已经被注册。
3、用户名没有被注册过，则判断两次输入的密码是否一致，一致的话则注册成功，否则给出对应的提示。
'''


def get_user(name):
    '''
    :param name:根据传入的用户名判断是否已注册
    :return: 已注册返回True，否则返回False
    '''
    d = ['张三', '李四', '王五']
    return True if name in d else False


def check_passwd(pwd, confirm_pwd):
    '''
    :param pwd:密码
    :param confirm_pwd:二次密码
    :return: 两次密码相同返回T，否则返回F
    '''
    return True if pwd == confirm_pwd else False


def reg():
    user_name = input('请输入用户名：')
    passwd = input('请输入密码：')
    confirm_passwd = input('请再次输入密码：')
    if get_user(user_name):
        print('很抱歉，该用户名已经被注册~')
    elif check_passwd(passwd, confirm_passwd):
        # 此处可以吧用户信息加入用户列表d中
        print('注册成功')
    else:
        print('密码不一致，请重新输入~')


if __name__ == '__main__':
    reg()
