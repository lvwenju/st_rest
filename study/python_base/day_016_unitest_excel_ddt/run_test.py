#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/9 22:22
# @Filename:run_test.py
# @Author:磐石
# @E-mail:mt_lwj@163.com
import unittest
from study.python_base.day_016_unitest_excel_ddt.test_fun import TestRegister, TestLogin
from HTMLTestRunnerNew import HTMLTestRunner

# 加载测试套
suite = unittest.TestSuite()

# 添加加载器
loader = unittest.TestLoader()
# 通过测试类添加
suite.addTest(loader.loadTestsFromTestCase(TestRegister))
suite.addTest(loader.loadTestsFromTestCase(TestLogin))

runner = HTMLTestRunner(open("report.html", "wb"), title=u"自动化测试-注册登录功能", description=u"注册登录自动化测试报告", tester="磐石")

runner.run(suite)

