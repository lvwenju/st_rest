#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/11 16:18
# @Filename:demo_02_cfg.py
# @Author:磐石
# @E-mail:mt_lwj@163.com

from configparser import ConfigParser

# 写配置信息到配置文件
# 创建解析对象
conf_write = ConfigParser()
# 设置section为logger，下面的键值对为字典
conf_write["logger"] = {"log_level": "info",
                        "log_path": r"./mylog.log",
                        "log_list": ["haha", 123]
                        }
with open("file_01.cfg", "w") as f:
    conf_write.write(f)

# 修改配置文件
conf_modify = ConfigParser()
conf_modify.read('file_01.cfg')

# 增加section
conf_modify.add_section("school")
# 逐个增加section下的key和value
conf_modify.set("school", "school_name", "name of school")
# 指定section增加或修改内容
conf_modify.set("logger", "log_size", "1024")
conf_modify.set("logger", "log_list", "['a', 1, 'c']")
conf_modify.set("school", "student_num", "5566")
conf_modify.set("school", "library", '{"library_name": "name of library", "book_types": ["junior", "middle"]}')
# 解析内容要写入配置文件
with open('file_02.cfg', 'w') as f:
    conf_modify.write(f)

# 配置文件读取
conf_read = ConfigParser()
# 解析文件内容，保存在解析对象中。文件不存在不会报错
conf_read.read('file_02.cfg')
# 获取配置文件所有的section，返回的是所有section组成的列表：['food']
print(conf_read.sections())

# 根据section获取下面的配置项。返回配置项组成的元组列表：[('meat', '["pig","cow"]'), ('fruit', '["watermelon","Strawberries"]')]
print(conf_read.items("logger"))

# 可以使用dict转换为字典。结果为：{'meat': '["pig","cow"]', 'fruit': '["watermelon","Strawberries"]', 'name': '"zhangsan"', 'age': '17'}
print(dict(conf_read.items(conf_read.sections()[0])))

# 读取section下的指定key对应的value值，返回的结果一律为字符串
log_size = conf_read.get("logger", "log_size")
log_list = conf_read.get("logger", "log_list")
print(type(log_size))  # 结果为：<class 'str'>
print(log_size)  # 结果为：1024
print(type(log_list))  # 结果为：<class 'str'>
print(log_list)  # 结果为：['a', 1, 'c']

# 根据section下的key以指定类型返回value值
size = conf_read.getint('logger','log_size')
print(type(size))
print(size)
# 如果指定类型错误或section不存在或key不存在均会报错
# size_01 = conf_read.getboolean('logger','log_size')
# size_01 = conf_read.getint('logger1','log_size')
# size_01 = conf_read.getint('logger','log_size1')