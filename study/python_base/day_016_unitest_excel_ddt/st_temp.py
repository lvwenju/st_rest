#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/10 20:44
# @Filename:st_temp.py
# @Author:磐石
# @E-mail:mt_lwj@163.com

import ddt
import unittest


@ddt.ddt
class TestCases(unittest.TestCase):

    @ddt.data(*[1, 23, 1, 4])
    def test_01(self, para):
        assert para == 1
