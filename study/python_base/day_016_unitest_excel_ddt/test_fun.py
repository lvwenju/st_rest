#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/9 21:46
# @Filename:test_fun.py
# @Author:磐石
# @E-mail:mt_lwj@163.com
import unittest
from .ddt import ddt, data
from study.python_base.day_015_unitest_excel.common_fun import *
from study.python_base.day_015_unitest_excel.register import register
from study.python_base.day_015_unitest_excel.login import login_check


@ddt
class TestLogin(unittest.TestCase):
    """测试：登录验证"""
    excel_login = OperateExcel("cases.xlsx", "login")
    cases = excel_login.case_data

    @data(*cases)
    def test_login(self, case):
        """登陆"""
        res = login_check(*eval(case["data"]))
        expected = eval(case["expected"])
        try:
            self.assertEqual(expected, res, msg="登陆期望：%s,预期结果：%s" % (expected, res))
        except AssertionError as e:
            self.excel_login.set_value("E" + str(case["case_id"] + 1), "failed")
            raise e
        else:
            self.excel_login.set_value("E" + str(case["case_id"] + 1), "pass")


@ddt
class TestRegister(unittest.TestCase):
    """测试：注册验证"""
    excel_register = OperateExcel("cases.xlsx", "register")
    cases = excel_register.case_data

    @data(*cases)
    def test_register(self, case):
        """注册"""
        res = register(*eval(case["data"]))
        expected = eval(case["expected"])
        try:
            self.assertEqual(expected, res, msg="登陆期望：%s,预期结果：%s" % (expected, res))
        except AssertionError as e:
            self.excel_register.set_value("E" + str(case["case_id"] + 1), "failed")
            raise e
        else:
            self.excel_register.set_value("E" + str(case["case_id"] + 1), "pass")
