#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/9 22:22
# @Filename:run_test.py
# @Author:磐石
# @E-mail:mt_lwj@163.com
import unittest
from study.python_base.day_015_unitest_excel.test_fun import TestRegister, TestLogin
from HTMLTestRunnerNew import HTMLTestRunner
from study.python_base.day_015_unitest_excel.common_fun import OperateExcel

# 加载测试套
suite = unittest.TestSuite()

# 添加测试数据
# 注册
excel_register = OperateExcel("cases.xlsx", "register")
for register_case in excel_register.case_data:
    suite.addTest(TestRegister("test_register", register_case))
# 登陆
excel_login = OperateExcel("cases.xlsx", "login")
for login_case in excel_login.case_data:
    suite.addTest(TestLogin("test_login", login_case))

runner = HTMLTestRunner(open("report.html", "wb"), title=u"自动化测试-注册登录功能", description=u"注册登录自动化测试报告", tester="磐石")

runner.run(suite)

