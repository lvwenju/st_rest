#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/9 21:46
# @Filename:test_fun.py
# @Author:磐石
# @E-mail:mt_lwj@163.com
import unittest
from study.python_base.day_015_unitest_excel.common_fun import *
from study.python_base.day_015_unitest_excel.register import register
from study.python_base.day_015_unitest_excel.login import login_check


class TestLogin(unittest.TestCase):
    """测试：登录验证"""
    excel_login = OperateExcel("cases.xlsx", "login")

    def __init__(self, method_name, test_data):
        self.test_data = test_data
        super().__init__(method_name)

    def test_login(self):
        """登陆"""
        res = login_check(*eval(self.test_data["data"]))
        expected = eval(self.test_data["expected"])
        try:
            self.assertEqual(expected, res, msg="登陆期望：%s,预期结果：%s" % (expected, res))
        except AssertionError as e:
            self.excel_login.set_value("E" + str(self.test_data["case_id"]+1), "failed")
            raise e
        else:
            self.excel_login.set_value("E" + str(self.test_data["case_id"]+1), "pass")


class TestRegister(unittest.TestCase):
    """测试：注册验证"""
    excel_login = OperateExcel("cases.xlsx", "register")

    def __init__(self, method_name, test_data):
        self.test_data = test_data
        super().__init__(method_name)

    def test_register(self):
        """注册"""
        res = register(*eval(self.test_data["data"]))
        expected = eval(self.test_data["expected"])
        try:
            self.assertEqual(expected, res, msg="登陆期望：%s,预期结果：%s" % (expected, res))
        except AssertionError as e:
            self.excel_login.set_value("E" + str(self.test_data["case_id"]+1), "failed")
            raise e
        else:
            self.excel_login.set_value("E" + str(self.test_data["case_id"]+1), "pass")
