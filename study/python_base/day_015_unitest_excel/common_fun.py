#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
# @Time:2020/2/9 21:33
# @Filename:common_fun.py
# @Author:磐石
# @E-mail:mt_lwj@163.com
import copy
from openpyxl import load_workbook


def create_or_modify_data(base_data, update_data, pop_data: list = []):
    """
    基于原始数据生成新的测试数据
    :param base_data: 基础数据（dict或list）
    :param update_data: 增加或更新的值（dict或list）
    :param pop_data: 需要删除的值（dict的键；list的值）
    :return: 修改后的dict或list
    """
    res = copy.deepcopy(base_data)
    try:
        if not isinstance(base_data, (dict, list)):
            raise TypeError("原始数据格式错误！【%s】" % base_data)
        if isinstance(base_data, dict):
            res.update(update_data)
            for key in pop_data:
                res.pop(key)
        if isinstance(base_data, list):
            res.append(update_data)
            for key in pop_data:
                res.remove(key)
        return res
    except TypeError as error_type:
        raise error_type
    except AttributeError as e:
        raise e


class OperateExcel:
    def __init__(self, filename, sheet_name=None):
        self.filename = filename
        self.wb = load_workbook(filename)
        if sheet_name is not None and sheet_name in self.wb.sheetnames:
            self.ws = self.wb.get_sheet_by_name(sheet_name)
        else:
            self.ws = self.wb.active

    @property
    def case_data(self):
        title_row = list(self.ws.rows)[0]
        case_rows = list(self.ws.rows)[1:]
        title_content = [cell.value for cell in title_row]
        case_contents = [[cell.value for cell in case_row] for case_row in case_rows]
        res = []
        for case in case_contents:
            res.append(dict(zip(title_content, case)))
        return res

    def set_value(self, cell_location, value, sheet_name=None):
        if sheet_name is None: sheet_name = self.ws.title
        self.ws_write = self.wb.get_sheet_by_name(sheet_name)
        self.ws_write[cell_location] = value
        self.wb.save(self.filename)


if __name__ == '__main__':
    excel = OperateExcel(r'E:\02AutoTest\st_rest\study\python_base\aaa.xlsx')
    for i in excel.case_data:
        print(i)
        print(type(i))
