# !/usr/bin/env  python
# -*- coding:utf8 -*-
#
# -------------------------
# @Author : panshi
# @Time : 2020/1/15
# @E-mail : mt_lwj@163.com
# -------------------------
import pytest
import requests


@pytest.fixture(autouse=True)
def func():
    print('func start...')
    yield 1
    print('func end...')


@pytest.fixture(scope='session',autouse=True)
def ses():
    print('before session')
    yield
    print('after session')


class Test_01(object):
    def test_01_01(self, func):
        res = requests.request('GET', 'http://127.0.0.1:8888/demo')
        print(res.status_code)
        print(func)
        assert 100 == res.status_code
        print(res.content.decode('utf8'))

    def test_01_02(self):
        assert 2 == 1

# if __name__ == '__main__':
